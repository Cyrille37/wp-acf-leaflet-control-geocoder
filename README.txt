=== Advanced Custom Fields: WpAcfLeafletControlGeocoder Field ===

Contributors: Cyrille37
Tags: geocoder, openstreetmap, leaflet
Requires at least: 5.0
Tested up to: 5.0
Stable tag: trunk
License: GPLv3 or later
License URI: http://www.gnu.org/licenses/gpl-3.0.html

An Wordpress ACF plugin field type that run Leaflet Control GeoCoder.

== Description ==

An Wordpress [Advanced Custom Fields (ACF)](https://www.advancedcustomfields.com) plugin field type that run [Leaflet Control GeoCoder](https://github.com/perliedman/leaflet-control-geocoder).

= Compatibility =

This ACF field type is compatible with:
* ACF 5
* ACF 4

Read file "Readme.md".

