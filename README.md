# WP ACF Leaflet Control Geocoder

**It's a draft ! not yet coded ...**

A Wordpress [Advanced Custom Fields (ACF)](https://www.advancedcustomfields.com) plugin field type that run [Leaflet Control GeoCoder](https://github.com/perliedman/leaflet-control-geocoder).

## Motivation

- avoir les 2 valeurs Lng/Lat permet des requêtes sur BBox
- Geocoder Photon permet interroger adresses.data.gouv.fr

## Sources of inspiration & knowledge

- [Creating a new ACF field type](https://www.advancedcustomfields.com/resources/creating-a-new-field-type/)
- Per Liedman's [Leaflet Control Geocoder](https://github.com/perliedman/leaflet-control-geocoder)
- Excellent [ACF OpenStreetMap Field, By Jörn Lund](https://github.com/mcguffin/acf-openstreetmap-field)
